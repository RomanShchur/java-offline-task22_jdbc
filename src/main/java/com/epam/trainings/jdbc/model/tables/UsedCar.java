package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "used_car")
public class UsedCar {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "celler_phone", fieldLength = 45)
  String cellerPhone;
  @Field(fieldName = "price", fieldLength = 11)
  int price;
  @Field(fieldName = "user_id", fieldLength = 11)
  int user_id;
  @Field(fieldName = "city", fieldLength = 45)
  String city;
  @Field(fieldName = "mileage", fieldLength = 11)
  int mileage;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getCellerPhone() {
    return cellerPhone;
  }
  public void setCellerPhone(String cellerPhone) {
    this.cellerPhone = cellerPhone;
  }
  public int getPrice() {
    return price;
  }
  public void setPrice(int price) {
    this.price = price;
  }
  public int getUser_id() {
    return user_id;
  }
  public void setUser_id(int user_id) {
    this.user_id = user_id;
  }
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }
  public int getMileage() {
    return mileage;
  }
  public void setMileage(int mileage) {
    this.mileage = mileage;
  }

  @Override public String toString() {
    return "UsedCar{" + "id=" + id + ", cellerPhone='" + cellerPhone + '\''
      + ", price=" + price + ", user_id=" + user_id + ", city='" + city + '\''
      + ", mileage=" + mileage + '}';
  }
}
