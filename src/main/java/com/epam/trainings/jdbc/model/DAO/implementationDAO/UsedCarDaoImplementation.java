package com.epam.trainings.jdbc.model.DAO.implementationDAO;

public class UsedCarDaoImplementation {
  private static final String FIND_ALL = "SELECT * FROM used_car";
  private static final String DELETE = "DELETE FROM used_car WHERE id=?";
  private static final String CREATE = "INSERT used_car"
    + " (celler_phone, price, city, mileage) "
    + "VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE used_car "
    + "SET celler_phone=?, price=?, city=?, mileage=?"
    + "WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM used_car WHERE id=?";
  private static final String FIND_BY_CITY = "SELECT * FROM used_car "
    + "WHERE city=?";


}
