package com.epam.trainings.jdbc.model;

import java.sql.*;

import static com.epam.trainings.jdbc.resources.jdbc_propertives.connectionURL;
import static com.epam.trainings.jdbc.resources.jdbc_propertives.username;
import static com.epam.trainings.jdbc.resources.jdbc_propertives.pass;

public final class DBconnection {
  private static Connection conn = null;
  public static Connection connectToDb() {
    if (conn == null) {
      try {
        conn = DriverManager.getConnection(connectionURL, username, pass);
      } catch (SQLException e) {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
      }
    }
    return conn;
  }
}
