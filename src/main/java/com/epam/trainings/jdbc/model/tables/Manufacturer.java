package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "manufacturer")
public class Manufacturer {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "brand", fieldLength = 11)
  String brand;
  @Field(fieldName = "country", fieldLength = 11)
  String country;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getBrand() {
    return brand;
  }
  public void setBrand(String brand) {
    this.brand = brand;
  }
  public String getCountry() {
    return country;
  }
  public void setCountry(String country) {
    this.country = country;
  }

  @Override public String toString() {
    return "Manufacturer{" + "id=" + id + ", brand='" + brand + '\''
      + ", country='" + country + '\'' + '}';
  }
}
