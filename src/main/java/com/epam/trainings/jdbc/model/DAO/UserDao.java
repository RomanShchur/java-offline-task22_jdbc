package com.epam.trainings.jdbc.model.DAO;

import com.epam.trainings.jdbc.model.tables.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao extends MainDao<Object, Integer> {
  List<User> fidByUsername(String username) throws SQLException;

  List<User> findByEmail(String email) throws SQLException;
}
