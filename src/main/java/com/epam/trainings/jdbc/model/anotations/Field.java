package com.epam.trainings.jdbc.model.anotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Field {
  String fieldName();
  int fieldLength();
}
