package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "engine_has_gearbox")
public class EngineGearbox {
  @TablePK()
  @Field(fieldName = "engine_id", fieldLength = 11)
  int engineId;
  @TablePK()
  @Field(fieldName = "gearbox_id", fieldLength = 11)
  int gearboxId;

  public int getEngineId() {
    return engineId;
  }
  public void setEngineId(int engineId) {
    this.engineId = engineId;
  }
  public int getGearboxId() {
    return gearboxId;
  }
  public void setGearboxId(int gearboxId) {
    this.gearboxId = gearboxId;
  }

  @Override public String toString() {
    return "EngineGearbox{" + "engineId=" + engineId + ", gearboxId="
      + gearboxId + '}';
  }
}
