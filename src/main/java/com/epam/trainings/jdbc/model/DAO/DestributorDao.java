package com.epam.trainings.jdbc.model.DAO;

import com.epam.trainings.jdbc.model.tables.Destributor;

import java.sql.SQLException;
import java.util.List;

public interface DestributorDao extends MainDao<Destributor, Integer> {
  List<Destributor> fidByTitle(String title) throws SQLException;

  List<Destributor> findByCity(String city) throws SQLException;
}
