package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "car_has_destributor")
public class CarDestributor {
  @TablePK()
  @Field(fieldName = "price", fieldLength = 11)
  int price;
  @Field(fieldName = "destributor_id", fieldLength = 11)
  int destributorId;

  public int getPrice() {
    return price;
  }
  public void setPrice(int price) {
    this.price = price;
  }
  public int getDestributorId() {
    return destributorId;
  }
  public void setDestributorId(int destributorId) {
    this.destributorId = destributorId;
  }

  @Override public String toString() {
    return "CarDestributor{" + "price=" + price + ", destributorId="
      + destributorId + '}';
  }
}
