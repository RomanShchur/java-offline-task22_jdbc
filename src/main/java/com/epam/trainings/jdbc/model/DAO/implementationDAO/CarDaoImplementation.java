package com.epam.trainings.jdbc.model.DAO.implementationDAO;

import com.epam.trainings.jdbc.model.DAO.CarDao;
import com.epam.trainings.jdbc.model.DBconnection;
import com.epam.trainings.jdbc.model.tables.Car;
import com.epam.trainings.jdbc.model.tables.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDaoImplementation implements CarDao {
  private static final String FIND_ALL = "SELECT * FROM `car`";
  private static final String DELETE = "DELETE FROM `car` WHERE id=?";
  private static final String CREATE = "INSERT INTO `car`"
    + " (model, used_car_id, production_year, length, width, height, seats, "
    + "doors, weight, full_weight) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE `car` "
    + "SET model=?, used_car_id=?, production_year=?, length=?, width=?, "
    + "height=?, seats=?, doors=?, weight=?, full_weight=?"
    + "WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM `car` WHERE id=?";
  private static final String FIND_BY_MODEL= "SELECT * FROM `car` "
    + "WHERE model=?";
  private static final String FIND_BY_PROD_YEAR = "SELECT * FROM `car` "
    + "WHERE production_year=?";

  @Override public List<Car> findAll() throws SQLException {
    List<Car> cars = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          cars.add((Car); /*new Transformer(User.class)
          .fromResultSetToEntity(resultSet))*/;
        }
      }
    }
    return cars;
  }

  @Override public Car findById(Integer integer) throws SQLException {
    Car entity=null;
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, integer);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity= (Car);
        /*new Transformer(EmployeeEntity.class)
            .fromResultSetToEntity(resultSet);*/
          break;
        }
      }
    }
    return entity;
  }

  @Override public int create(Car entity) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int update(Car entity, Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int delete(Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1,integer);
      return ps.executeUpdate();
    }
  }

  @Override public List<Car> fidByModel(String model) throws SQLException {
    List<Car> cars = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_MODEL)) {
      ps.setString(1, model);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          cars.add((Car);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return cars;
  }

  @Override public List<Car> findByProductionYear(String productionYear)
    throws SQLException {
    List<Car> cars = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_PROD_YEAR)) {
      ps.setString(1, productionYear);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          cars.add((Car);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return cars;
  }
}
