package com.epam.trainings.jdbc.model.DAO;

import java.sql.SQLException;
import java.util.List;

public interface MainDao<T, ID> {
  List<T> findAll() throws SQLException;

  T findById(ID id) throws SQLException;

  int create(T entity) throws SQLException;

  int update(T entity, ID id) throws SQLException;

  int delete(ID id) throws SQLException;
}
