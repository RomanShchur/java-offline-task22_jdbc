package com.epam.trainings.jdbc.model.DAO;

import com.epam.trainings.jdbc.model.tables.Car;

import java.sql.SQLException;
import java.util.List;

public interface CarDao extends MainDao<Car, Integer> {
  List<Car> fidByModel(String model) throws SQLException;

  List<Car> findByProductionYear(String productionYear) throws SQLException;
}
