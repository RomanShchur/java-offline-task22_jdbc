package com.epam.trainings.jdbc.model.DAO;

import com.epam.trainings.jdbc.model.tables.Engine;

import java.sql.SQLException;
import java.util.List;

public interface EngineDao extends MainDao<Engine, Integer> {
  List<Engine> fidByFuelType(String fuelType) throws SQLException;
}
