package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "engine")
public class Engine {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "fuel", fieldLength = 45)
  String fuel;
  @Field(fieldName = "volume", fieldLength = 11)
  double volume;
  @Field(fieldName = "cylinders", fieldLength = 11)
  int cylinders;
  @Field(fieldName = "valves", fieldLength = 11)
  int valves;
  @Field(fieldName = "power", fieldLength = 11)
  int power;
  @Field(fieldName = "maximum_speed", fieldLength = 11)
  int maxSpeed;
  @Field(fieldName = "fuel_consumption", fieldLength = 11)
  double fuelConsumption;
  @TablePK()
  @Field(fieldName = "car_id", fieldLength = 11)
  int carID;
  @TablePK()
  @Field(fieldName = "car_manufacturer_id", fieldLength = 11)
  int carManufacturerId;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getFuel() {
    return fuel;
  }
  public void setFuel(String fuel) {
    this.fuel = fuel;
  }
  public double getVolume() {
    return volume;
  }
  public void setVolume(double volume) {
    this.volume = volume;
  }
  public int getCylinders() {
    return cylinders;
  }
  public void setCylinders(int cylinders) {
    this.cylinders = cylinders;
  }
  public int getValves() {
    return valves;
  }
  public void setValves(int valves) {
    this.valves = valves;
  }
  public int getPower() {
    return power;
  }
  public void setPower(int power) {
    this.power = power;
  }
  public int getMaxSpeed() {
    return maxSpeed;
  }
  public void setMaxSpeed(int maxSpeed) {
    this.maxSpeed = maxSpeed;
  }
  public double getFuelConsumption() {
    return fuelConsumption;
  }
  public void setFuelConsumption(double fuelConsumption) {
    this.fuelConsumption = fuelConsumption;
  }
  public int getCarID() {
    return carID;
  }
  public void setCarID(int carID) {
    this.carID = carID;
  }
  public int getCarManufacturerId() {
    return carManufacturerId;
  }
  public void setCarManufacturerId(int carManufacturerId) {
    this.carManufacturerId = carManufacturerId;
  }

  @Override public String toString() {
    return "Engine{" + "id=" + id + ", fuel='" + fuel + '\'' + ", volume="
      + volume + ", cylinders=" + cylinders + ", valves=" + valves + ", power="
      + power + ", maxSpeed=" + maxSpeed + ", fuelConsumption="
      + fuelConsumption + ", carID=" + carID + ", carManufacturerId="
      + carManufacturerId + '}';
  }
}
