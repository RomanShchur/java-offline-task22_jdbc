package com.epam.trainings.jdbc.model.DAO.implementationDAO;

import com.epam.trainings.jdbc.model.DAO.GearboxDao;
import com.epam.trainings.jdbc.model.DBconnection;
import com.epam.trainings.jdbc.model.tables.Car;
import com.epam.trainings.jdbc.model.tables.Engine;
import com.epam.trainings.jdbc.model.tables.Gearbox;
import com.epam.trainings.jdbc.model.tables.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GearboxDaoImplementation implements GearboxDao {
  private static final String FIND_ALL = "SELECT * FROM gearbox";
  private static final String DELETE = "DELETE FROM gearbox WHERE id=?";
  private static final String CREATE = "INSERT gearbox"
    + " (gearbox_type, is_mechanical, gear_number, wheel_drive) "
    + "VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE gearbox "
    + "SET gearbox_type=?, is_mechanical=?, gear_number=?, wheel_drive=?"
    + "WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM gearbox WHERE id=?";
  private static final String FIND_BY_ISMECHANICAL = "SELECT * FROM gearbox "
    + "WHERE is_mechanical=?";

  @Override public List<Gearbox> findAll() throws SQLException {
    List<Gearbox> gearboxes = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          gearboxes.add((Gearbox); /*new Transformer(User.class)
          .fromResultSetToEntity(resultSet))*/;
        }
      }
    }
    return gearboxes;
  }

  @Override public Gearbox findById(Integer integer) throws SQLException {
    Gearbox entity=null;
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, integer);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity= (Gearbox);
        /*new Transformer(EmployeeEntity.class)
            .fromResultSetToEntity(resultSet);*/
          break;
        }
      }
    }
    return entity;
  }

  @Override public int create(Car entity) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int update(Car entity, Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int delete(Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1,integer);
      return ps.executeUpdate();
    }
  }

  @Override public List<Gearbox> fidIsMechanical(Boolean isMechanical) throws SQLException {
    List<Gearbox> gearboxes = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ISMECHANICAL)) {
      ps.setString(1, isMechanical);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          gearboxes.add((Gearbox);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return gearboxes;
  }
}
