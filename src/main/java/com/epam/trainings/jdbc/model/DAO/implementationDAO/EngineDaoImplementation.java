package com.epam.trainings.jdbc.model.DAO.implementationDAO;

import com.epam.trainings.jdbc.model.DAO.EngineDao;
import com.epam.trainings.jdbc.model.DBconnection;
import com.epam.trainings.jdbc.model.tables.Car;
import com.epam.trainings.jdbc.model.tables.Destributor;
import com.epam.trainings.jdbc.model.tables.Engine;
import com.epam.trainings.jdbc.model.tables.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EngineDaoImplementation implements EngineDao {
  private static final String FIND_ALL = "SELECT * FROM engine";
  private static final String DELETE = "DELETE FROM engine WHERE id=?";
  private static final String CREATE = "INSERT engine"
    + " (fuel, volume, cylinders, valves, power, maximum_speed, "
    + "fuel_consumption, car_id, car_manufacturer_id) "
    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE engine "
    + "SET title=?, fuel=?, volume=?, cylinders=?, valves=?, power=?, "
    + "maximum_speed=?, fuel_consumption=?, car_id=?, car_manufacturer_id=?"
    + "WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM engine WHERE id=?";
  private static final String FIND_BY_FUEL_TYPE= "SELECT * FROM engine "
    + "WHERE fuel=?";

  @Override public List<Engine> findAll() throws SQLException {
    List<Engine> engines = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          engines.add((Car); /*new Transformer(User.class)
          .fromResultSetToEntity(resultSet))*/;
        }
      }
    }
    return engines;
  }

  @Override public Engine findById(Integer integer) throws SQLException {
    Engine entity=null;
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, integer);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity= (Engine);
        /*new Transformer(EmployeeEntity.class)
            .fromResultSetToEntity(resultSet);*/
          break;
        }
      }
    }
    return entity;
  }

  @Override public int create(Car entity) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int update(Car entity, Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int delete(Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1,integer);
      return ps.executeUpdate();
    }
  }

  @Override public List<Engine> fidByFuelType(String model) throws SQLException {
    List<Engine> engines = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_FUEL_TYPE)) {
      ps.setString(1, model);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          engines.add((Engine);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return engines;
  }
}
