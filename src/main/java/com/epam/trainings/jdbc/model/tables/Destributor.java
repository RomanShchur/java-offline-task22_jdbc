package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "destributor")
public class Destributor {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "title", fieldLength = 45)
  String title;
  @Field(fieldName = "address", fieldLength = 45)
  String address;
  @Field(fieldName = "phone", fieldLength = 45)
  String phone;
  @Field(fieldName = "city", fieldLength = 45)
  String city;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getAddress() {
    return address;
  }
  public void setAddress(String address) {
    this.address = address;
  }
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  public String getCity() {
    return city;
  }
  public void setCity(String city) {
    this.city = city;
  }

  @Override public String toString() {
    return "Destributor{" + "id=" + id + ", title='" + title + '\''
      + ", address='" + address + '\'' + ", phone='" + phone + '\'' + ", city='"
      + city + '\'' + '}';
  }
}
