package com.epam.trainings.jdbc.model.DAO.implementationDAO;

import com.epam.trainings.jdbc.model.DAO.UserDao;
import com.epam.trainings.jdbc.model.DBconnection;
import com.epam.trainings.jdbc.model.tables.User;

import javax.xml.transform.Transformer;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImplementation implements UserDao {
  private static final String FIND_ALL = "SELECT * FROM `user`";
  private static final String DELETE = "DELETE FROM `user` WHERE id=?";
  private static final String CREATE = "INSERT INTO `user` "
    + " (username, email, password) VALUES (?, ?, ?)";
  private static final String UPDATE = "UPDATE `user` "
    + "SET username=?, email=?, password=? WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM `user` WHERE id=?";
  private static final String FIND_BY_USERNAME = "SELECT * FROM `user` "
    + "WHERE username=?";
  private static final String FIND_BY_EMAIL = "SELECT * FROM `user` "
    + "WHERE email=?";

  public List<User> findAll() throws SQLException {
    List<User> users = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          users.add((User); /*new Transformer(User.class)
          .fromResultSetToEntity(resultSet))*/;
        }
      }
    }
    return users;
  }

  public User findById(Integer integer) throws SQLException {
    User entity=null;
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, integer);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity= (User);
        /*new Transformer(EmployeeEntity.class)
            .fromResultSetToEntity(resultSet);*/
          break;
        }
      }
    }
    return entity;
  }

  public int create(User entity) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  public int update(User entity, Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  public int delete(Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1,1);
      return ps.executeUpdate();
    }
  }

  public List<User> fidByUsername(String username) throws SQLException {
    List<User> users = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_USERNAME)) {
      ps.setString(1, username);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          users.add((User);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return users;
  }

  public List<User> findByEmail(String email) throws SQLException {
    List<User> users = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_EMAIL)) {
      ps.setString(1, email);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          users.add((User);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return users;
  }
}
