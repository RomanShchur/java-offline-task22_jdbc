package com.epam.trainings.jdbc.model.DAO;

import com.epam.trainings.jdbc.model.tables.Gearbox;

import java.sql.SQLException;
import java.util.List;

public interface GearboxDao extends MainDao<Gearbox, Integer> {
  List<Gearbox> fidIsMechanical(Boolean machanical) throws SQLException;
}
