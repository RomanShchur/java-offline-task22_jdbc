package com.epam.trainings.jdbc.model.DAO.implementationDAO;

import com.epam.trainings.jdbc.model.DAO.DestributorDao;
import com.epam.trainings.jdbc.model.DBconnection;
import com.epam.trainings.jdbc.model.tables.Car;
import com.epam.trainings.jdbc.model.tables.Destributor;
import com.epam.trainings.jdbc.model.tables.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DestributorDaoImplementation implements DestributorDao {
  private static final String FIND_ALL = "SELECT * FROM destributor";
  private static final String DELETE = "DELETE FROM destributor WHERE id=?";
  private static final String CREATE = "INSERT destributor"
    + " (title, address, phone, city) VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE destributor "
    + "SET title=?, address=?, phone=?, city=?"
    + "WHERE id=?";
  private static final String FIND_BY_ID = "SELECT * FROM destributor WHERE id=?";
  private static final String FIND_BY_TITLE= "SELECT * FROM destributor "
    + "WHERE title=?";
  private static final String FIND_BY_CITY = "SELECT * FROM destributor "
    + "WHERE city=?";

  @Override public List<Destributor> findAll() throws SQLException {
    List<Destributor> destribs = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          destribs.add((Car); /*new Transformer(User.class)
          .fromResultSetToEntity(resultSet))*/;
        }
      }
    }
    return destribs;
  }

  @Override public Destributor findById(Integer integer) throws SQLException {
    Destributor entity=null;
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
      ps.setInt(1, integer);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          entity= (Destributor);
        /*new Transformer(EmployeeEntity.class)
            .fromResultSetToEntity(resultSet);*/
          break;
        }
      }
    }
    return entity;
  }

  @Override public int create(Car entity) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int update(Car entity, Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
      ps.setInt(1, User.getUsername());
      ps.setString(2,entity.getEmail());
      ps.setString(3,entity.getPassword());
      return ps.executeUpdate();
    }
  }

  @Override public int delete(Integer integer) throws SQLException {
    Connection conn = DBconnection.connectToDb();
    try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
      ps.setInt(1,integer);
      return ps.executeUpdate();
    }
  }

  @Override public List<Destributor> fidByTitle(String model) throws SQLException {
    List<Destributor> destribs = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_TITLE)) {
      ps.setString(1, model);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          destribs.add((Destributor);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return destribs;
  }

  @Override public List<Destributor> findByCity(String productionYear)
    throws SQLException {
    List<Destributor> destribs = new ArrayList<>();
    Connection connection = DBconnection.connectToDb();
    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_CITY)) {
      ps.setString(1, productionYear);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          destribs.add((Destributor);/* new Transformer(User.class)
          .fromResultSetToEntity(resultSet));*/
        }
      }
    }
    return destribs;
  }
}
