package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "Car")
public class Car {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "model", fieldLength = 45)
  String model;
  @Field(fieldName = "used_car_id", fieldLength = 11)
  int UsedCar;
  @Field(fieldName = "production_year", fieldLength = 11)
  String productionYear;
  @Field(fieldName = "length", fieldLength = 11)
  int length;
  @Field(fieldName = "wodth", fieldLength = 11)
  int width;
  @Field(fieldName = "height", fieldLength = 11)
  int heigth;
  @Field(fieldName = "seats", fieldLength = 11)
  int seats;
  @Field(fieldName = "doors", fieldLength = 11)
  int doors;
  @Field(fieldName = "weight", fieldLength = 11)
  int weight;
  @Field(fieldName = "full_weight", fieldLength = 11)
  int fullWeight;
  @Field(fieldName = "manufacturer_id", fieldLength = 11)
  int manufacturerId;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getModel() {
    return model;
  }
  public void setModel(String model) {
    this.model = model;
  }
  public int getUsedCar() {
    return UsedCar;
  }
  public void setUsedCar(int usedCar) {
    UsedCar = usedCar;
  }
  public String getProductionYear() {
    return productionYear;
  }
  public void setProductionYear(String productionYear) {
    this.productionYear = productionYear;
  }
  public int getLength() {
    return length;
  }
  public void setLength(int length) {
    this.length = length;
  }
  public int getWidth() {
    return width;
  }
  public void setWidth(int width) {
    this.width = width;
  }
  public int getHeigth() {
    return heigth;
  }
  public void setHeigth(int heigth) {
    this.heigth = heigth;
  }
  public int getSeats() {
    return seats;
  }
  public void setSeats(int seats) {
    this.seats = seats;
  }
  public int getDoors() {
    return doors;
  }
  public void setDoors(int doors) {
    this.doors = doors;
  }
  public int getWeight() {
    return weight;
  }
  public void setWeight(int weight) {
    this.weight = weight;
  }
  public int getFullWeight() {
    return fullWeight;
  }
  public void setFullWeight(int fullWeight) {
    this.fullWeight = fullWeight;
  }
  public int getManufacturerId() {
    return manufacturerId;
  }
  public void setManufacturerId(int manufacturerId) {
    this.manufacturerId = manufacturerId;
  }

  @Override public String toString() {
    return "Car{" + "id=" + id + ", model='" + model + '\'' + ", UsedCar="
      + UsedCar + ", productionYear='" + productionYear + '\'' + ", length="
      + length + ", width=" + width + ", heigth=" + heigth + ", seats=" + seats
      + ", doors=" + doors + ", weight=" + weight + ", fullWeight=" + fullWeight
      + ", manufacturerId=" + manufacturerId + '}';
  }
}
