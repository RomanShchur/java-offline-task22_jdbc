package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "gearbox")
public class Gearbox {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "gearbox_type", fieldLength = 45)
  String gearboxType;
  @Field(fieldName = "is_mechanical", fieldLength = 1)
  boolean isMechanical;
  @Field(fieldName = "gear_number", fieldLength = 11)
  int gearNumber;
  @Field(fieldName = "wheel_drive", fieldLength = 45)
  String wheelDrive;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getGearboxType() {
    return gearboxType;
  }
  public void setGearboxType(String gearboxType) {
    this.gearboxType = gearboxType;
  }
  public boolean isMechanical() {
    return isMechanical;
  }
  public void setMechanical(boolean mechanical) {
    isMechanical = mechanical;
  }
  public int getGearNumber() {
    return gearNumber;
  }
  public void setGearNumber(int gearNumber) {
    this.gearNumber = gearNumber;
  }
  public String getWheelDrive() {
    return wheelDrive;
  }
  public void setWheelDrive(String wheelDrive) {
    this.wheelDrive = wheelDrive;
  }

  @Override public String toString() {
    return "Gearbox{" + "id=" + id + ", gearboxType='" + gearboxType + '\''
      + ", isMechanical=" + isMechanical + ", gearNumber=" + gearNumber
      + ", wheelDrive='" + wheelDrive + '\'' + '}';
  }
}
