package com.epam.trainings.jdbc.model.tables;

import com.epam.trainings.jdbc.model.anotations.Field;
import com.epam.trainings.jdbc.model.anotations.Table;
import com.epam.trainings.jdbc.model.anotations.TablePK;

@Table(tableName = "user")
public class User {
  @TablePK()
  @Field(fieldName = "id", fieldLength = 11)
  int id;
  @Field(fieldName = "username", fieldLength = 45)
  String username;
  @Field(fieldName = "email", fieldLength = 45)
  String email;
  @Field(fieldName = "password", fieldLength = 45)
  String password;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  @Override public String toString() {
    return "User{" + "id=" + id + ", username='" + username + '\'' + ", email='"
      + email + '\'' + ", password='" + password + '\'' + '}';
  }
}
