package com.epam.trainings.jdbc.model.DAO;

import com.epam.trainings.jdbc.model.tables.UsedCar;

public interface UsedCarDao extends MainDao<UsedCar, Integer> {

}
