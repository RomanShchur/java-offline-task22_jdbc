//package com.epam.trainings.jdbc.view;
//
//import java.sql.ResultSet;
//import java.sql.ResultSetMetaData;
//import java.sql.Statement;
//
//public class SomeClass {
//  public static void main(String[] args) {
//    String query = "SELECT * FROM car";
//    Statement st = conn.createStatement();
//    ResultSet rs = st.executeQuery(query);
//
//    ResultSetMetaData metadata = rs.getMetaData();
//    int columnCount = metadata.getColumnCount();
//    for (int i = 1; i <= columnCount; i++) {
//      System.out.print(metadata.getColumnName(i) + ",\t");
//    }
//    System.out.println();
//    while (rs.next()) {
//      String row = "";
//      for (int i = 1; i <= columnCount; i++) {
//        row += rs.getString(i) + ", \t";
//      }
//      System.out.println(row);
//    }
//  }
//}
